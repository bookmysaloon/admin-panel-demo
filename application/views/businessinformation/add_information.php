<?php
if($this->session->flashdata('message')) {
  $message = $this->session->flashdata('message');

  ?>
<div class="alert alert-<?php echo $message['class']; ?>">
<button class="close" data-dismiss="alert" type="button">×</button>
<?php echo $message['message']; 

		?>
</div>
<?php
}
        $sess_id=$this->session->userdata('logged_in');
		$u_id=$sess_id['id'];
?>


<html>
<h1 align="center">Add Business</h1>
<boady>

    <table align="center">
    	<?php echo form_open("Business_information_control/information_add"); ?>
    	
    <tr><td>Primary phone number for your business</td><td><input type="text" name="phone_number" required> </td></tr>	
    <tr><td>Business name</td><td><input type="text" name="business_name" required> </td></tr>	
    <tr><td>Business owner's first name</td><td><input type="text" name="first_name" required> </td></tr>	
    <tr><td>Business owner's last name</td><td><input type="text" name="last_name" required> </td></tr>	
    <tr><td>Your Email</td><td><input type="email" name="email"  required> </td></tr>
    <tr><td>Categories that best describe your business</td><td><input type="text" name="categories"  required> </td></tr>
    <tr><td>Street address</td><td><input type="text" name="street_address" required> </td></tr>	
    <tr><td>City</td><td><input type="text" name="city" required> </td></tr>	
    <tr><td>State</td><td><input type="text" name="state" required> </td></tr>	
    <tr><td>Zip code</td><td><input type="text" name="zip_code" required> </td></tr>	
    <tr><td>Year Established</td><td><input type="text" name="year_established" required> </td></tr>
	
     <input type="hidden" name="created_by" value="<?php echo $u_id; ?>">	
	 
    <tr><td><input type="submit" value="Submit" id="reg" > </td></tr>	
        
          <?php echo form_close(); ?>
    </table>
    
 <?php
    if(($this->session->flashdata('item'))) {
      $message = $this->session->flashdata('item');
      ?>
      <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
       <?php
       }
       ?>

</body>
</html>