<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business_information_control extends CI_Controller {

	  public function __construct()
	   {
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->model('business_information_model','information');
		$this->load->helper('url');
	    $this->load->database();
	    $this->load->library('session');
	
	      if(!$this->session->userdata('logged_in'))
	        {
			redirect(base_url());
		    }
		}
	
	
	public function information_details()
	       {   
		
		            $template['page'] ='businessinformation/business_information';
					$template['page_title'] = "business information";
					$template['data'] = $this->information->view_business_information();
					$this->load->view('businessinformation/business_information',$template);
	       }
	
   		    public function information_add()
				 {
						$template['page'] = 'businessinformation/add_information';
						$template['page_title'] = 'Add information';
						
						if($_POST){
							 $data = $_POST;
							 $result = $this->information->add_information($data);
							 $this->session->set_flashdata('message',array('message' => 'Add information Details successfully','class' => 'success'));
						}
						//$this->load->view('admin-template',$template);
						$this->load->view('businessinformation/add_information',$template);
				  }
		  	 public function information_edit()
				{	
					$template['page'] = "businessinformation/edit_information";
					$template['page_title'] = "Edit Information";	
					
					$id = $this->uri->segment(3);
					$template['data'] = $this->information->get_single_information($id);
					if($_POST) {
					  $data = $_POST;
					  $result = $this->information->edit_information($data, $id);
					  $this->session->set_flashdata('message', array('message' => 'information Details Updated successfully','class' => 'success'));
					  redirect(base_url().'Business_information_control/information_details');
					}
					else
					{
						$this->load->view('businessinformation/edit_information',$template); 
					}
						
				}			 
             public function information_delete()
					{
					   $id = $this->uri->segment(3);		   
					   $result = $this->information->delete_information($id);		   
					   $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));
					   redirect(base_url().'Business_information_control/information_details');
					}  

}
