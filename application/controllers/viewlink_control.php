<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class viewlink_control extends CI_Controller {

	  public function __construct()
	   {
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->model('viewlink_model','home');
		$this->load->helper('url');
	    $this->load->database();
	    $this->load->library('session');
	    //session_start();
		
		if(!$this->session->userdata('logged_in'))
	        {
			redirect(base_url());
		    }
		}

	          public function view_page()
				{
						$template['page'] ='viewlink/viewpage';
					    $template['page_title'] = "view link";
					   // $template['a'] = $this->home->details_business();
					    $this->load->view('viewlink/viewpage',$template);
				}
	        
		      
			
	
}