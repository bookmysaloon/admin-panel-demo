<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

	          public function __construct()
					   {
						parent::__construct();
						
						$this->load->helper('form');
						$this->load->model('Business_model','business');
						$this->load->helper('url');
						$this->load->database();
						$this->load->library('session');
					
						  if(!$this->session->userdata('logged_in'))
							{
							redirect(base_url());
							}
						}
	
	           
	          public function add_business()
					   {
						$this->load->view('Business/add_business');
					   }
	          public function business_action()
					   {
						$template['page'] = 'Business/add_business';
						$template['page_title'] = 'Add Business';
						
						if($_POST){
							 $data = $_POST;
							 $result = $this->business->add_business($data);
							 $this->session->set_flashdata('message',array('message' => 'Add Cartype Details successfully','class' => 'success'));
						}
						//$this->load->view('admin-template',$template);
						$this->load->view('Business/add_business',$template);
					   }
	          public function business_details()
					   {   
			
						$template['page'] ='Business/business_details';
						$template['page_title'] = "business details";
						$template['a'] = $this->business->details_business();
						//foldername and pagename///
						$this->load->view('Business/business_details',$template);
					   }
	          public function edit_business()
						{	
						$template['page'] = 'Business/edit_business';
						$template['page_title'] = "Edit Business";	
						
						$id = $this->uri->segment(3);
						$template['data'] = $this->business->get_single_business($id);
						if($_POST) {
						  $data = $_POST;
						  $result = $this->business->business_edit($data, $id);
						  $this->session->set_flashdata('message', array('message' => 'Business Details Updated successfully','class' => 'success'));
						  redirect(base_url().'Business/business_details');
						}
						else
						{
							$this->load->view('Business/edit_business',$template); 
						}
							
						}		
               public function business_delete()
						{
						   $id = $this->uri->segment(3);		   
						   $result = $this->business->delete_business($id);		   
						   $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));
						   redirect(base_url().'business/business_details');
						}  
}
