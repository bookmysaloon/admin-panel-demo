<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_login extends CI_Controller {

	  public function __construct()
	   {
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->model('Login_model','home');
		$this->load->helper('url');
	    $this->load->database();
	    $this->load->library('session');
	    //session_start();
		}
	          public function index()
						{
							$this->load->view('login_form');
						}
	         public function loginaction()
				{
					
					$data=$_POST;
					$result=$this->home->login($data);
					
					if($result)
					{
                  
						$sess_values=array(
										  'id'=>$result->id,
										  'email'=>$result->email
										   );
						
					 
						$this->session->set_userdata('logged_in',$sess_values);
						
						$this->session->set_flashdata('item', array('message' => 'Logged in successfully','class' => 'success')); 
					  
					   
					   redirect(base_url().'viewlink_control/view_page');
					}
					else
					{
					   $this->session->set_flashdata('item', array('message' => 'Username or Password Incorrect','class' => 'error')); 
					   redirect(base_url().'User_login/');
					}	
					
				}	
					
		      
			
	
}