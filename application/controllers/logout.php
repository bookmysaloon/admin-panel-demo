<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		
		$this->load->helper('form');
		$this->load->helper('url');
	    $this->load->library('session');
	
	      if(!$this->session->userdata('logged_in'))
	        {
			redirect(base_url());
		    }
		}
	
	function index() 
			{
				$this->session->unset_userdata('logged_in');
				session_destroy();
				//redirect('User_login');
				redirect(base_url());
			}
}
?>