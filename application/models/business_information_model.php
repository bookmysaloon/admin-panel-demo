<?php
class business_information_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }
	
		 function view_business_information()
		         {
					$this->db->select('yp_business_information.id as id, yp_business_information.business_name, yp_business_information.email,  yp_business_information.categories, yp_business_information.city, yp_business_information.state, yp_business_information.zip_code, yp_business_information.created_by, yp_users.first_name');
					$this->db->from('yp_business_information');
					$this->db->join('yp_users', 'yp_business_information.created_by = yp_users.id','left');
					$this->db->group_by("yp_business_information.id"); 
					
					
					$query = $this->db->get();
					$result = $query->result();
					return $result;	
		         }
		  function add_information($data)	
				 {	
				  $result = $this->db->insert('yp_business_information', $data); 
				  return "Success";
				 }
		  function get_single_information($id)
				 {
				  $query = $this->db->where('id', $id);
				  $query = $this->db->get('yp_business_information');
				  $result = $query->row();
				  return $result;
				 }
		  function edit_information($data, $id)
				 { 
				  $this->db->where('id', $id);
				  $result = $this->db->update('yp_business_information', $data); 
				  return "Success";	 
				 }	
          function delete_information($id) 
				 {
						 var_dump($id);
						 $this->db->where('id', $id);
						 $result = $this->db->delete('yp_business_information'); 
						 
						 if($result) {
							 return "Success";
						 }
						 else {
							 return "Error";
						 }
				}
				
				
				
}
?>